# ChillButton

This repo will hold the microservices that make the ChillButton possible.

Chillers unite! Press the button and the chilling is yours!

## To Run the Application
### Install dependencies
`npm install && npm install -g gulp`
### Run
`gulp`