# Auth service

## Intro
Authentication: A user provides a username and password. If name/pass is valid, DB returns id and an id+token is generated and passed to the DB.

Authorization: Checks id+token/request pairs against the database. Returns Authorized [true or false] and id+token and request.

The Auth service will need to read/write access to the database.

Note: This Auth service will not handle registration. Another registration service will be required.

## Initialization
```
node index.js --dbURL=[dbURL]
```

## API

Authentication:
---------------
```
/authenticate/?name=[username]&pass=[password]
```
Sending a credential pair will trigger a call to check the database. (later to include hashwords)
If valid, the userid from the db will be prepended to a randomly generated token with '+' and returned.
The token is sent to the db for checking later.


Authorization:
--------------
```
/authorize/?req=[request]&token=[token]
```
Sending a request and token will trigger a call to the database and return a determination of the following form:
```
{
  authorized:bool, // true or false is the request authorized
  token: token // the id+token sent with the request
  request: request
}
```
