// auth/index.js
var argv = require('optimist').argv;
const dbURL = argv.dbURL;
if (!dbURL) {
	throw new Error("Specify a DB url --dbURL=http://etc");
}
const port = process.env.port || 7799;
const fetch = require('node-fetch');
const express = require(`express`);
const cors = require(`cors`)
const app = new express();
app.use(cors());
app.set('json spaces', 2);


app.get('/',(req,res)=>{
	res.send(`
		HI! THIS IS THE AUTH SERVICE! ^___^
    WHO ARE YOU?
	`)
})


app.get('/authenticate/',(req,res)=>{
  if (!req.query.name) {
   return res.status(403).json({error:"Invalid args"});
 }

  	fetch(`${dbURL}/authenticate/?name=${req.query.name}&pass=${req.query.pass}`) // returns authenticated, id, token
  	.then(function(resp) {
          return resp.json();
      })
  	.then(response=>{
  		if (response.authenticated == false) {
  			return res.status(200).json(
  				{
  					authenticated: false
  				});
  		}
      if (response.authenticated == true){
       return  res.status(200).json({
  				response: response
  			})
  		}

  	})
    .catch(function(err) {
        console.log(err);
        res.status(500).json(err);
    });
  })

app.get('/authorize/',(req,res)=>{
    if (!req.query.token) {
     return res.status(403).json({error:"Invalid args"});
   }

   const id = req.query.token.split('zz')[0];
    	fetch(`${dbURL}/authorize/?id=${id}&token=${req.query.token}&req=${req.query.req}`) // returns authenticated, id, token
    	.then(function(resp) {
            return resp.json();
        })
    	.then(response=>{
    		if (response.authorized == false) {
    			return res.status(200).json(
    				{
    					authorized: false
    				});
    		}
        if (response.authorized == true){
         return  res.status(200).json({
              authorized:response.authorized, // true or false is the request authorized
              token: req.query.token, // the id+token sent with the request
              request: req.query.req
    			})
    		}

    	})
      .catch(function(err) {
          console.log(err);
          res.status(500).json(err);
      });
    })

app.listen(port,()=>console.log("AUTH APP OPERATIONAL"))
