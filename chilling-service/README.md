# Chilling Service

## Intro
Lets other services know the status of buttons.
Other service can ping it, or register URLs to be pinged when a change occurs.

## Initialization
```
node index.js [port]
```

## API
`/status/:id?auth=[token]`
Gets the status of the button with the specified ID in the following format.
`id`: The ID of the requested button
`token`: The auth token for the request

```javascript
{
	id:string, // the button's ID
	authValidated:bool, // true or false if the auth was OK
	isChilling:bool, // a boolean representing if the individual is chilling
}
```

`/status/register/:buttonID?auth=[token]&url=[url]`
Registers a URL to be called whenever a change occurs with the specified button.
`buttonID`: The button's ID.
`token`: The token for the request.
`url`: The URL for the button to be called.

This request returns the following values:
```javascript
{
	id:string, // the button's ID
	authValidated:bool, // true or false if the auth was OK
	registered:bool, // a boolean representing if the registration was successful
}
```

URLs will receive GET requests with the following data.
`[url]?id=[id]&isChilling=[isChilling]`
