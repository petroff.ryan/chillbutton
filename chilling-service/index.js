var argv = require('optimist').argv;
const dbURL = argv.dbURL;
if (!dbURL) {
	throw new Error("Specify a DB url --dbURL=http://etc");
}
const fetch = require('node-fetch');
const express = require(`express`);
const cors = require(`cors`)
const app = new express();
const port = process.env.port || 7778;
const chance = new require('chance')();

app.use(cors());
app.set('json spaces', 2);

app.use(express.static('public'));
const listeners = [];
app.get('/subscribe/:id',(req,res)=>{
    // console.log("Receive subscribe request...",res);
    const guid = chance.guid();
    const cbURL = "http://localhost:7778/" + guid;
    const id = req.params.id;
    app.use(cbURL,(req,res)=>{
        console.log("Got callback...",req,query);
    })

    console.log("Issue request...",cbURL)
    fetch(`${dbURL}/status/subscribe/${id}?url=${cbURL}`)
        .then(function (res) {
            return res.json();
        })

        .then(response => {
            console.log("Subscribed...")
        })
        .catch(function(err) {
            console.log(err);
            res.status(500).json(err);
        });
    res.status(420).json({subscribed:true});
    listeners.push({
        id: req.params.id,
        url: req.query.url
    });

});

/* Find whether a particular ID is chilling. */
app.get('/:id',(req,res)=>{
	fetch(`${dbURL}/status/${req.params.id}`)
	.then(function(res) {
        return res.json();
    })
	.then(response=>{
		if (response.isChilling) {
			res.json(
				{
					isChilling:response.isChilling
				});
		} else {
			res.json({
				response
			})
		}
	})
  .catch(function(err) {
      console.log(err);
      res.status(500).json(err);
  });
})

app.listen(port,()=>console.log(`Chilling service listening on port ${port}`));
