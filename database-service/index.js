const express = require(`express`);
const cors = require(`cors`);
const crypto = require(`crypto`);
const app = new express();
const port = process.env.port || 7777;
app.use(cors());
app.use(express.static('public'));
const soCalledDatabase = {
	buttons:{
		"abcd-1234":{
			isChilling:true,
			lastUpdated:new Date(),
			userid: "efgh-1234"
		}
	},
	users:{
		"efgh-1234":{
			name:"leroy",
			password:"jenkins", // this should be hashed
			buttons:["abcd-1234"],
			temptoken:"temporarytokentimeslalala~" // hash of the time plus name
		}
	}
};

app.get('/',(req,res)=>{
	res.send(`
		HI! THIS IS THE DATABASE SERVICE.
	`)
});

app.get('/user/:id',(req,res)=>{

});

const fetch = require('node-fetch');
const statusListeners = [];

app.get('/status/subscribe/:id',(req,res)=>{
    res.status(420).json({subscribed:true});
    statusListeners.push({
        id: req.params.id,
        url: req.query.url
    })
    console.log("receive subscribe request")

    setTimeout(()=>{
        console.log("issue response auto...",req.query.url)
        fetch(`${req.query.url}?chilling=false`)
            .then(function (res) {
                console.log(res.status);
                return res.json();
            })
            .then(response => {

            })
            .catch(function(err) {
                console.log(err);
                res.status(500).json(err);
            });
    },1000)
});

app.get('/status/:id',(req,res)=>{
	if (!req.params || !req.query) {
		return res.status(403).json({error:"Invalid args"});
	}
	const id = req.params.id;
	const auth = req.query.auth;
	const isChilling = req.query.isChilling;
	const buttons = soCalledDatabase.buttons;
	if (!buttons[id]) {
		return res.status(500).json({
			error:`No button with ID ${id}`
		});
	}

	const button = buttons[id];
    const user = soCalledDatabase.users[button.userid];
	return res.status(200).json({
		id,
		isChilling:button.isChilling,
		lastUpdated:button.lastUpdated,
		user
	})
})

app.post('/status/:id',(req,res)=>{
	if (!req.params || !req.query) {
		return res.status(403).send("Uhhhh");
	}
	const id = req.params.id;
	const isChilling = req.query.isChilling;
	const auGth = req.query.auth;

	if (isChilling === undefined || isChilling === "undefined") {
		throw new Error("Undefined is chilling parameter...",isChilling);
	}


	const buttons = soCalledDatabase.buttons;

	if (!buttons[id]) {
		buttons[id] = {};
	}

	const isChillingBool = isChilling == "true";


	const button = buttons[id];
	button.isChilling = isChillingBool;
	button.lastUpdated = new Date();

	res.status(300)
	.send({
		didUpdate:true
	});
})


app.get('/authenticate/',(req,res)=>{
	if (!req.params || !req.query) {
		return res.status(403).json({error:"Invalid args"});
	}
	const name = req.query.name;
	const pass = req.query.pass;

	const users = soCalledDatabase.users;
	if (!users[name]) {
		return res.status(500).json({
			error:`No user with name ${name}`
		});
	}
	const user = users[name];
	if((name == user.name) && (pass == user.password)){
		// and return authenticated, id, token
		// and put token in 'db'
		var newtoken = user.userid + "zz" + crypto.randomBytes(20).toString('hex');
    soCalledDatabase.users[name].temptoken = newtoken;

		return res.status(200).json({
			authenticated: true,
			id: user.userid,
			token: user.temptoken
		})
	} else {
		return res.status(200).json({
			authenticated: false
		});
	}

})

app.get('/authorize/',(req,res)=>{
	if (!req.params || !req.query) {
		return res.status(403).json({error:"Invalid args"});
	}
	const id = req.query.id;
	const token = req.query.token;
	const reqreq = req.query.req;

	const ids = soCalledDatabase.users.ids;
	if (!ids[id]) {
		return res.status(500).json({
			error:`No such ID ${id}`
		});
	}

	const user = soCalledDatabase.users[ids[id]];
	if (token == user.temptoken) {
		return res.status(200).json({
			authorized: true,
			id:id,
			token:token,
			request:reqreq
		})
  } else {
		return res.status(500).json({
			error: `Wrong token - ${token} is not ${user.temptoken}`
		});
	}
})

app.listen(port,()=>console.log(`DB service listening on Dracula ${port}`));
