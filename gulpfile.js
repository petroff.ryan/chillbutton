const exec = require('child_process').exec;
const spawn = require('child_process').spawn;
const gulp = require('gulp');
const devMode = process.env.NODE_ENV !== "PRODUCTION";
if (devMode) {
	console.info("Running in dev mode");
} else {
	console.info("Running in production mode... so watch out for that");
}

const createProcess = (path,cb)=>{
	const child = exec(path);
	child.stdout.pipe(process.stdout);
	child.stderr.pipe(process.stderr);
};

const initializeNodeService = (folder, cb)=>{
	createProcess(`cd ${folder} && npm install && ${devMode ? `npm run start-dev` : `npm start`}`);
}

gulp.task('chilling service',(cb)=>initializeNodeService('chilling-service'));
gulp.task('receiver service',(cb)=>initializeNodeService('receiver-service'));
gulp.task('auth service',(cb)=>initializeNodeService('auth-service'));
gulp.task('database service',(cb)=>initializeNodeService('database-service'));
gulp.task('visitor view service',(cb)=>initializeNodeService('visitor-view-service'));


gulp.task('default',[
	'chilling service',
	'receiver service',
	'auth service',
	'database service',
	'visitor view service',
]);