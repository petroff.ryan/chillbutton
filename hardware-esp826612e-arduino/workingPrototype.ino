// This is the MK4 Chillbutton code

// To be deployed to an ESP8266 12E
// with a Button, some Neopixels, and Wifi
// Pressing the button activates chilling mode
//      Sends notice of chilling status to chilling service
//      Green lights and half rainbow on reply from inbound chillers
// Pressing it again deactivates chilling mode
//      Fades to red then fades to black
// Lights flash blue if there's no wifi
// prompting you to use the captive web portal
// to put set wifi up
// EXTRA POINTS: OTA for extra fun

// TIMING NOTES:
// delay(ms) pauses the sketch for a given number of
// milliseconds and allows WiFi and TCP/IP tasks to run.
// Remember that there is a lot of code that needs to run on the chip
// besides the sketch when WiFi is connected. WiFi and TCP/IP libraries
// get a chance to handle any pending events each time the loop() function completes
// OR when delay is called. If you have a loop somewhere in your sketch that takes a
// lot of time (>50ms) without calling delay, you might consider adding a call to
// delay function to keep the WiFi stack running smoothly.

#include <ESP8266WiFi.h>          //https://github.com/esp8266/Arduino

//needed for library
#include <DNSServer.h>
#include <ESP8266WebServer.h>
#include <WiFiManager.h>          //https://github.com/tzapu/WiFiManager

//for LED status
#include <Ticker.h>
Ticker ticker;

#include <NeoPixelBus.h>
#include <NeoPixelAnimator.h>

const int buttonPin = D3;
uint32_t buttonState;
int lastButtonState = HIGH;
unsigned long lastDebounceTime = 0;  // the last time the output pin was toggled
unsigned long debounceDelay = 60;    // the debounce time; increase if the output flickers
int IS_CHILLING = 0;
const int GreenLEDPin = D1;

const uint16_t PixelCount = 8; // make sure to set this to the number of pixels in your strip
// PixelPin = D4 // ignored by Esp8266, kept for documentation
NeoPixelBus<NeoGrbFeature, NeoEsp8266Uart800KbpsMethod> strip(PixelCount);

// NeoPixel animation time management object
NeoPixelAnimator animations(PixelCount, NEO_CENTISECONDS);

// I'm not sure these are necessary but they may need to be uncommented
// #include <ESP8266WiFiMulti.h>
//  ESP8266WiFiMulti WiFiMulti;

#include <ESP8266HTTPClient.h>
#define USE_SERIAL Serial

void TriggerChillingStatus()
{
        digitalWrite(GreenLEDPin, IS_CHILLING);
        USE_SERIAL.print("Chilling is on! IS_CHILLING = 1 \n");

        HTTPClient http;
        USE_SERIAL.print("[HTTP] begin...\n");
        http.begin("http://192.168.2.38:7779/update/abcd-1234?auth=auth&isChilling=true"); //HTTP
        USE_SERIAL.printf("[HTTP] GET... isChilling=true");
        // start connection and send HTTP header
        int httpCode = http.GET();

        // httpCode will be negative on error
        if(httpCode > 0) {
            // HTTP header has been send and Server response header has been handled
            USE_SERIAL.printf("[HTTP] GET... code: %d\n", httpCode);

            // file found at server
            if(httpCode == HTTP_CODE_OK) {
                String payload = http.getString();
                USE_SERIAL.println(payload);
            }
        } else {
            USE_SERIAL.printf("[HTTP] GET... failed, error: %s\n", http.errorToString(httpCode).c_str());
        }

        http.end();
}

void TriggerUNChillingStatus()
{
        digitalWrite(GreenLEDPin, IS_CHILLING);
        USE_SERIAL.print("Abort Chilling! IS_CHILLING = 0 ! \n");

        HTTPClient http;
        USE_SERIAL.print("[HTTP] begin...\n");
        http.begin("http://192.168.2.38:7779/update/abcd-1234?auth=auth&isChilling=false"); //HTTP
        USE_SERIAL.printf("[HTTP] GET... isChilling=false");
        // start connection and send HTTP header
        int httpCode = http.GET();

        // httpCode will be negative on error
        if(httpCode > 0) {
            // HTTP header has been send and Server response header has been handled
            USE_SERIAL.printf("[HTTP] GET... code: %d\n", httpCode);

            // file found at server
            if(httpCode == HTTP_CODE_OK) {
                String payload = http.getString();
                USE_SERIAL.println(payload);
            }
        } else {
            USE_SERIAL.printf("[HTTP] GET... failed, error: %s\n", http.errorToString(httpCode).c_str());
        }

        http.end();
}

void SetRandomSeed()
{
    uint32_t seed;

    // random works best with a seed that can use 31 bits
    // analogRead on a unconnected pin tends toward less than four bits
    seed = analogRead(0);
    delay(1);

    for (int shifts = 3; shifts < 31; shifts += 3)
    {
        seed ^= analogRead(0) << shifts;
        delay(1);
    }

    Serial.println(seed);
    randomSeed(seed);
}

void SetupAnimationSet()
{
// NeoPixelAnimation
// This example will randomly pick a new color for each pixel and animate
// the current color to the new color over a random small amount of time, using
// a randomly selected animation curve.
// It will repeat this process once all pixels have finished the animation
//
// This will demonstrate the use of the NeoPixelAnimator extended time feature.
// This feature allows for different time scales to be used, allowing slow extended
// animations to be created.
//
// This will demonstrate the use of the NeoEase animation ease methods; that provide
// simulated acceleration to the animations.
//
// It also includes platform specific code for Esp8266 that demonstrates easy
// animation state and function definition inline.  This is not available on AVR
// Arduinos; but the AVR compatible code is also included for comparison.
//
// The example includes some serial output that you can follow along with as it
// does the animation.
//
    // setup some animations
    for (uint16_t pixel = 0; pixel < PixelCount; pixel++)
    {
        const uint8_t peak = 128;

        // pick a random duration of the animation for this pixel
        // since values are centiseconds, the range is 1 - 4 seconds
        uint16_t time = random(100, 400);

        // each animation starts with the color that was present
        RgbColor originalColor = strip.GetPixelColor(pixel);
        // and ends with a random color

        RgbColor targetColor;

        //LETS ADD THE CHILLBUTTON CHARM HACK HERE
        if (IS_CHILLING == 0)
        {
          targetColor = RgbColor(random(peak), 0, 0);
        }
        else
        {
          targetColor = RgbColor(0, random(peak), 0);
        }


        // with the random ease function
        AnimEaseFunction easing;

        switch (random(3))
        {
        case 0:
            easing = NeoEase::CubicIn;
            break;
        case 1:
            easing = NeoEase::CubicOut;
            break;
        case 2:
            easing = NeoEase::QuadraticInOut;
            break;
        }

        // we must supply a function that will define the animation, in this example
        // we are using "lambda expression" to define the function inline, which gives
        // us an easy way to "capture" the originalColor and targetColor for the call back.
        //
        // this function will get called back when ever the animation needs to change
        // the state of the pixel, it will provide a animation progress value
        // from 0.0 (start of animation) to 1.0 (end of animation)
        //
        // we use this progress value to define how we want to animate in this case
        // we call RgbColor::LinearBlend which will return a color blended between
        // the values given, by the amount passed, hich is also a float value from 0.0-1.0.
        // then we set the color.
        //
        // There is no need for the MyAnimationState struct as the compiler takes care
        // of those details for us
        AnimUpdateCallback animUpdate = [=](const AnimationParam& param)
        {
            // progress will start at 0.0 and end at 1.0
            // we convert to the curve we want
            float progress = easing(param.progress);

            // use the curve value to apply to the animation
            RgbColor updatedColor = RgbColor::LinearBlend(originalColor, targetColor, progress);
            strip.SetPixelColor(pixel, updatedColor);
        };

        // now use the animation properties we just calculated and start the animation
        // which will continue to run and call the update function until it completes
        animations.StartAnimation(pixel, time, animUpdate);
    }
}


void tick()
{
  //toggle state
  int state = digitalRead(BUILTIN_LED);  // get the current state of GPIO1 pin
  digitalWrite(BUILTIN_LED, !state);     // set pin to the opposite state
}

//gets called when WiFiManager enters configuration mode
void configModeCallback (WiFiManager *myWiFiManager) {
  Serial.println("Entered config mode");
  Serial.println(WiFi.softAPIP());
  //if you used auto generated SSID, print it
  Serial.println(myWiFiManager->getConfigPortalSSID());
  //entered config mode, make led toggle faster
  ticker.attach(0.2, tick);
}

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);

  //set led pin as output
  pinMode(BUILTIN_LED, OUTPUT);
  // start ticker with 0.5 because we start in AP mode and try to connect
  ticker.attach(0.6, tick);

  //set up button
  pinMode(buttonPin, INPUT);
  buttonState = digitalRead(buttonPin);
  Serial.println("Button state is: " + buttonState);

  pinMode(GreenLEDPin, OUTPUT);


   // initialize neopixel strip
   strip.Begin();
   strip.Show();

  SetRandomSeed();
  for (uint16_t pixel = 0; pixel < PixelCount; pixel++)
    {
        RgbColor color = RgbColor(random(255), random(255), random(255));
        strip.SetPixelColor(pixel, color);
    }

    Serial.println();
    Serial.println("Running...");

  //WiFiManager
  //Local intialization. Once its business is done, there is no need to keep it around
  WiFiManager wifiManager;
  //reset settings - for testing
  //wifiManager.resetSettings();

  //set callback that gets called when connecting to previous WiFi fails, and enters Access Point mode
  wifiManager.setAPCallback(configModeCallback);

  //fetches ssid and pass and tries to connect
  //if it does not connect it starts an access point with the specified name
  //here  "AutoConnectAP"
  //and goes into a blocking loop awaiting configuration
  if (!wifiManager.autoConnect("ChillButtonAP")) {
    Serial.println("failed to connect and hit timeout");
    //reset and try again, or maybe put it to deep sleep
    ESP.reset();
    delay(1000);
  }

  //if you get here you have connected to the WiFi
  Serial.println("connected...yeey :)");
  ticker.detach();
  //keep LED on
  digitalWrite(BUILTIN_LED, LOW);
}

void loop() {
  // put your main code here, to run repeatedly:

          int reading = digitalRead(buttonPin);
          if (reading != lastButtonState)
          {
            lastDebounceTime = millis();
          }
          if ((millis() - lastDebounceTime) > debounceDelay) {
           // whatever the reading is at, it's been there for longer
           // than the debounce delay, so take it as the actual current state:

           // if the button state has changed:
           if (reading != buttonState) {
               buttonState = reading;
            // only toggle the LED if the new button state is HIGH
             if (buttonState == LOW) {
                IS_CHILLING = !IS_CHILLING;
            if (IS_CHILLING == 1)
            {
                 TriggerChillingStatus();
                 // ADD FAST GREEN ANIMATION HERE
                 for (uint16_t pixel = 0; pixel < PixelCount; pixel++)
    {
        RgbColor color = RgbColor(0, 255, 0);
        strip.SetPixelColor(pixel, color);
    }
    strip.Show();
    SetupAnimationSet();
            }
            else
            {
                 TriggerUNChillingStatus();
                 for (uint16_t pixel = 0; pixel < PixelCount; pixel++)
    {
        RgbColor color = RgbColor(255, 0, 0);
        strip.SetPixelColor(pixel, color);
    }
    strip.Show();
    SetupAnimationSet();
            }
                 }
                }
               }
            lastButtonState= reading;

          Serial.print("Button state is: ");
          Serial.print(buttonState);
          Serial.println();
          Serial.print("Chilling state is: ");
          Serial.print(IS_CHILLING);
          Serial.println();





   if (animations.IsAnimating())
    {
        // the normal loop just needs these two to run the active animations
        animations.UpdateAnimations();
        strip.Show();

    }
    else
    {
        Serial.println();
        Serial.println("Setup Next Set...");
        // example function that sets up some animations
        SetupAnimationSet();
          //buttonState = digitalRead(buttonPin);
          //Serial.println("Button state is: " + buttonState);
          //Serial.println(buttonState);
    }

}
