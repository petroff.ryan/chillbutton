# Receiver Service

## Intro
The service receives information from the button about what it's state is.
Buttons call this service via HTTP to tell the application what the button is doing, but this module can't talk back to the button.

## Initialization
```
node index.js [port]
```

## API
`update/:id?auth=[auth]&status=["true" or "false"]

Updates the server with the state of a button.
Request receive a JSON object in response,
```javascript
{
	id:string, // the button's ID
	authValidated:bool, // true or false if the auth was OK
	didUpdate:bool // did the update happen successfully
}
``` 