const argv = require('optimist').argv;
const dbURL = argv.dbURL;
if (!dbURL) {
	throw new Error("Specify a DB url --dbURL=http://etc");
}
const fetch = require('node-fetch');

const express = require(`express`);
const cors = require(`cors`)
const app = new express();
const port = process.env.port || 7779;
app.use(express.static('public'));
app.use(cors());


app.get('/update/:id',(req,res)=>{
    console.log("Getting request...",req.params, req.query);
    const isChilling = (req.query.isChilling === "true" ? true : false);
	fetch(`${dbURL}/status/${req.params.id}?auth=TODOADDAUTH&isChilling=${isChilling}`, { method: 'POST' })
    .then(function(raw) {
        return raw.json();
    }).then(function(response) {
        res.json(response);
    });
})

// Call callback here
app.listen(port,"0.0.0.0",()=>console.log(`Receiver service listening on port ${port}`));