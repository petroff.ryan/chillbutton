import React from 'react';
import reactDOM from 'react-dom'
import { Main } from './components/Main';

const render = (props)=>{
    reactDOM.render(
        <div>
            <Main {...props}/>
        </div>,
        document.getElementById('AppContainer'));
};

const props = window.__PRELOADED_STATE__;
const socket = window.io();
socket.emit(`REGISTER_CHILLER`,props.id);
socket.on(`CHILLER_STATUS_UPDATE`, render);
console.log("Proppa props...",props);
render(props);