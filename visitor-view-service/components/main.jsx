import React from 'react';

const z = a=>console.log(a);
export const Main = ({noSuchID,id,isChilling,user})=>(<section>
    {noSuchID ? <div>
        Our most sincere apology... there's no-one with that ID. ID: {id}
    </div> : <div>
        <h2>Chiller Report</h2>
        <div className="jumbotron">
            <h3>
                {user.name} is
            </h3>
            <h1>
                {isChilling ? ":) CHILLING :)" : ":( NOT CHILLING :("}
            </h1>
        </div>
    </div>}
</section>);