require('babel-register');
const argv = require('optimist').argv;
const dbURL = argv.dbURL;
const authURL = argv.authURL;
const chillerURL = argv.chillerURL;

if (!dbURL) {
    throw new Error("Specify a DB url --dbURL=http://etc");
}

if (!authURL) {
    throw new Error("Specify an authURL url --authURL=http://etc");
}

if (!chillerURL) {
    throw new Error("Specify an chillerURL url --chillerURL=http://etc");
}

require('./server').default(dbURL,authURL,chillerURL);
