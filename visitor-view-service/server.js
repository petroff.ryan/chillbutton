import React from 'react';
import { Main } from './components/main.jsx';
import http from 'http';
import webpack from 'webpack';
import webpackConfig from './webpack.config'
import webpackDevMiddleware from 'webpack-dev-middleware';
import webpackHotMiddleware from "webpack-hot-middleware";
import Chance from 'chance';
const compiler = webpack(webpackConfig);
const chance = new Chance();

export default (dbURL,authURL,chillerURL)=>{
    const reactDomServer = require('react-dom/server');

    const fetch = require('node-fetch');
    const express = require(`express`);

    const cors = require(`cors`)
    const app = express();
    const server = http.Server(app);
    const io = require('socket.io')(server);
    const port = process.env.port || 7780;

    app.set('view engine', 'ejs');

    app.use(express.static('public'));
    app.use(cors());

    app.use(webpackDevMiddleware(compiler, {
        noInfo: true,
        publicPath: webpackConfig.output.publicPath,
    }));

    app.use(webpackHotMiddleware(compiler, {
        'log': false,
        'path': '/__webpack_hmr',
        'heartbeat': 10 * 1000
    }));

    app.get('/:id',(req,res)=>{
        const id = req.params.id;
        fetch(`${dbURL}/status/${id}`)
            .then(function(res) {
                return res.json();
            })
            .then((response)=>{
                const noSuchID = !!response.error; //todo... more dependable indicator
                response.user = response.user || {}; // todo.. fix deliberate hack
                const props = {
                    isChilling:response.isChilling,
                    name:response.name,
                    noSuchID,
                    id,
                    user:response.user

                };
                const html = reactDomServer.renderToString(
                    <div>
                        <Main {...props}></Main>
                    </div>
                );

                res.render('index',{
                    html,
                    preloadedState:JSON.stringify(props).replace(/</g, '\\u003c')
                });
            })
            .catch(function(err) {
                console.log(err);
                res.status(500).json(err);
            });

    });

    app.get('/',(req,res)=>{
        res.send("This is the visitor view service. Go to /:id or something.")
    });

    app.get('/:id',(req,res)=>{
        res.send(`Its a view for ${req.params.id}`);
    });

    server.listen(port,"0.0.0.0",()=>console.log(`Visitor view service is listing on Port ${port}`));

    io.on('connection', function (socket) {
        console.log("Alright!");
        socket.on('REGISTER_CHILLER', function (id) {
            console.log(id);
            const guid = chance.guid();
            const cbURL = "http://localhost:7780/" + guid;
            console.log("Issue request...",cbURL)
            fetch(`${chillerURL}/subscribe/${id}?url=${cbURL}`)
                .then(function (res) {
                    return res.json();
                })

                .then(response => {
                    console.log("Subscribed...")
                })
                .catch(function(err) {
                    console.log(err);
                    res.status(500).json(err);
                });
            });
    });
}

// io.on('connection',(info)=>{
//     console.log("someone connected...",info);
// });